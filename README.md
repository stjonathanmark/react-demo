# Description
This is a demo app that is created with Node, Express, and React.

# Instructions
1. To run this application you will need to download and install **NodeJs**. [Download NodeJs](https://nodejs.org)
2. Open the NodeJs command line tool and set it's path to the **'Application Root'** folder where you placed it on your computer after downloading/cloning repository
3. Type and run the command **'npm install'** in the NodeJs command line tool to install all the dependencies from the **'package.json'** file
4. Type and run the command **'npm run-script build'** in the NodeJs command line tool to compile to the React files into bundles and place them in the newly created **'build'** folder
5. Type and run the command **'node app'** in the NodeJs command line tool to start node server hosting the website
6. Open any browser and type ['http://localhost:5000']('http://localhost:5000') in the address bar and press enter
7. Enjoy!!!!!!!