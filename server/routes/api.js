var express = require('express');
var data = require('../data/dataSource');

var router = express.Router();

router.route('/persons')
    .get((req, res) => {
        let persons = data.getPersons();
        res.json({ message: 'All persons were retrieved successfully.', persons: persons });
    })
    .post((req, res) => {
        data.createPerson(req.body.person);
        res.status(201).send({ newId: req.body.person.id, message: 'Person was created sucessfully.' });
    })
    .put((req, res) => {
        data.updatePerson(req.body.person);
        res.send({ message: 'Person was updated successfully.' })
    });

router.route('/persons/:id')
    .get((req, res) => {
        let person = data.getPerson(req.params.id);

        if (!person) return res.status(404).send({ message: 'Person not found' });

        res.send({ message: 'Person was retrieved successfully.', person: person });
    })
    .delete((req, res) => {
        data.deletePerson(req.params.id);
        res.send({ message: 'Person was deleted successfully' });
    });

module.exports = router;