let persons = [
    { id: 1, firstName: 'John', lastName: 'Doe' },
    { id: 2, firstName: 'Jane', lastName: 'Doe' }
];

let nextId = 3;

let getPersonById = (id) => {
    let person = null;

    persons.forEach((per) => {
        if (per.id == id) {
            person = per;
            return;
        }
    });

    return person;
}

let getPersonIndex = (id) => {
    var person = getPersonById(id);
    return persons.indexOf(person);
}

module.exports = {
    getPersons: () => {
        return persons;
    },
    getPerson: (id) => {
        return getPersonById(id);
    },
    createPerson: (person) => {
        person.id = nextId++;
        persons.push(person);
    },
    updatePerson: (person) => {
        let idx = getPersonIndex(person.id);
        persons.splice(idx, 1, person);
    },
    deletePerson: (id) => {
        let idx = getPersonIndex(id);
        persons.splice(idx, 1);
    }
}