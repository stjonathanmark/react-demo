var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var api = require('./server/routes/api');

var app = express();

app.use(express.static(path.join(__dirname, 'build')));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
})

app.use('/api', api);

app.get("*", function (req, res, next) {
    var returnDefault = true;
    var fileType = [".ico", ".js", ".map", ".css", ".png", ".jpg", ".gif"];

    fileType.forEach((val, idx, arr) => {
        if (req.url.endsWith(val)) {
            returnDefault = false;
            return;
        }
    });

    if (returnDefault) {
        res.sendFile(path.join(__dirname, "build/index.html"));
    } else {
        next();
    }
});

app.listen(5000, function () {
    console.log('Listening on port 5000');
});