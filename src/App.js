import React, { Component } from 'react';
import { Route } from 'react-router-dom'
import Layout from './views/Layout';
import './App.css';
import Home from './views/Home';
import Create from './views/Create';
import Update from './views/Update';

class App extends Component {
  render() {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/create' component={Create} />
        <Route path='/update/:id' component={Update} />
      </Layout>
    );
  }
}

export default App;
