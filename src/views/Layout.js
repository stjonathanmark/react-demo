import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'

class Layout extends Component {
    render() {
        return (
            <div id='wrapper'>
                <nav id="mainNavbar">
                    <NavLink to="/">Home</NavLink> &nbsp;&nbsp;&nbsp;
                    <NavLink to="/create">Create New Peson</NavLink>
                </nav>
                <main id="mainContent">
                    <section id="mainSection">
                        {this.props.children}
                    </section>
                </main>
            </div>
        );
    }
}

export default Layout;