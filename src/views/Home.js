import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'

class Home extends Component {
    constructor() {
        super();
        this.state = {
            persons: []
        };
    }

    componentWillMount() {
        this.getPersons();
    }

    getPersons = () => {
        fetch('/api/persons')
            .then(res => res.json())
            .then(response => this.setState({ persons: response.persons }));
    }

    deletePerson = (e) => {
        var id = e.target.getAttribute('data-id');
        fetch(`/api/persons/${id}`, { method: 'DELETE', })
            .then(() => this.getPersons());
    }
    render() {
        let tbody = null;

        if (!this.state.persons || !this.state.persons.length) {
            tbody = (
                <tr>
                    <td align="center" colspan="3">No People Found</td>
                </tr>
            )
        } else {
            tbody = this.state.persons.map((person, index) => {
                return (
                    <tr>
                        <td>{person.firstName}</td>
                        <td>{person.lastName}</td>
                        <td>
                            <NavLink to={`update/${person.id}`}>Update</NavLink> &nbsp;&nbsp;
                            <a href="/" class="delete" onClick={this.deletePerson} data-id={person.id}>Delete</a>
                        </td>
                    </tr>
                );
            });
        }

        return (
            <section id='Home'>
                <h1>Behold the People</h1>
                <table>
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tbody}
                    </tbody>
                </table>
            </section>
        );
    }
}

export default Home;