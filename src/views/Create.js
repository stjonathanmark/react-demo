import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

class Create extends Component {
    constructor() {
        super();
        this.state = {
            created: false,
            person: {
                firstName: '',
                lastName: ''
            }
        };
    }

    onChange = (e) => {
        let { name, value } = e.target;

        let person = JSON.parse(JSON.stringify(this.state.person));
        person[name] = value;

        this.setState({ person: person });
    }

    createPerson = () => {
        fetch('/api/persons', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({ person: this.state.person })
        })
            .then(() => { this.setState({ created: true }) });
    }


    render() {
        if (this.state.created) {
            return (<Redirect to="/" />)
        }

        return (
            <section id="create">
                <h1>Create New Person</h1>
                <table>
                    <tr>
                        <td class="ctrlName">First Name:</td>
                        <td>
                            <input type="text" name="firstName" value={this.state.person.firstName} onChange={this.onChange} />
                        </td>
                    </tr>
                    <tr>
                        <td class="ctrlName">Last Name:</td>
                        <td>
                            <input type="text" name="lastName" value={this.state.person.lastName} onChange={this.onChange} />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button onClick={this.createPerson}>Create</button>
                        </td>
                    </tr>
                </table>
            </section>
        )
    }
}

export default Create;