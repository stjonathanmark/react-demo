import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

class Update extends Component {
    constructor(props) {
        super();
        this.state = {
            updated: false,
            person: {
                id: props.match.params.id,
                firstName: '',
                lastName: ''
            }
        };

    }

    componentWillMount() {
        fetch(`/api/persons/${this.state.person.id}`)
            .then(res => res.json())
            .then(response => this.setState({ person: response.person }));
    }

    onChange = (e) => {
        let { name, value } = e.target;

        let person = JSON.parse(JSON.stringify(this.state.person));
        person[name] = value;

        this.setState({ person: person });
    }

    updatePerson = () => {
        fetch('/api/persons', {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({ person: this.state.person })
        })
            .then(() => { this.setState({ updated: true }) });
    }

    render() {
        if (this.state.updated) {
            return (<Redirect to="/" />)
        }

        return (
            <section id="update">
                <h1>Update Person</h1>
                <table>
                    <tr>
                        <td class="ctrlName">First Name:</td>
                        <td>
                            <input type="text" name="firstName" value={this.state.person.firstName} onChange={this.onChange} />
                        </td>
                    </tr>
                    <tr>
                        <td class="ctrlName">Last Name:</td>
                        <td>
                            <input type="text" name="lastName" value={this.state.person.lastName} onChange={this.onChange} />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button onClick={this.updatePerson}>Update</button>
                        </td>
                    </tr>
                </table>
            </section>
        )
    }
}

export default Update;